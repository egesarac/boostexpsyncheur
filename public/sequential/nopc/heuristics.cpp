#include <iostream>
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "global.h"
#include "naive.h"
#include <random>
#include <fstream>
#include <cmath>
#include "omp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>


using namespace std;
using namespace std::chrono;

#define ALGORITHM PL

int checkInverse(int *a, int* iap, int* ia, int N, int P) {
  for (int p = 0; p < P; p++) {
    for (int i = 0; i < N; i++) {
      int target = a[p + i * P];
      
      int found = 0;
      for (int iaptr = iap[p * (N + 1) + target]; iaptr < iap[p * (N + 1) + target + 1]; ++iaptr) {
	int incoming = ia[p * N + iaptr];
	if (i == incoming) {
	  found = 1;
					break;
				}
			}

			if (!found) {
				cout << "something is wrong " << i << " goes to " << target << " with " << p << " but it is not in the inverse automata\n";
				exit(1);
			}
		}
	}

	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			for (int iaptr = iap[p * (N + 1) + i]; iaptr < iap[p * (N + 1) + i + 1]; ++iaptr) {
				int source = ia[p * N + iaptr];
				if (a[p + source * P] != i) {
					cout << "something is wrong " << i << " has " << source << " in inverse automata but it " << source << " goes to " << a[p + source * P] << " with " << p << "\n";
					exit(1);
				}
			}
		}
	}

	return 0;
}

int main(int argc, char** argv) {
	if ((!(argc == 7 || (argc == 6 && (strcmp(argv[2], "cerny") == 0 || strcmp(argv[2], "bactrian") == 0 || strcmp(argv[2], "dromedary") == 0 || strcmp(argv[2], "fix5") == 0 || strcmp(argv[2], "fix4") == 0 || strcmp(argv[2], "fix3") == 0 || strcmp(argv[2], "doubleCerny") == 0))))) {
	  cout << "Usage: " << argv[0] << " no_states alphabet_size rand_seed threshold level_coef memory_usage\n" << endl;
	  cout << "Usage: " << argv[0] << " no_states alphabet_size file_name threshold level_coef memory_usage\n" << endl;
	  cout << "Usage: " << argv[0] << " no_states slowly_sync_automata_type threshold level_coef memory_usage\n" << endl;
	  cout << "slowly_sync_automata_type: cerny, bactrian, dromedary, fix5, fix4, fix3, doubleCerny\n" << endl;
	  return 1;
	}
	
	int N = atoi(argv[1]); //state sayisi
	int P;// = atoi(argv[2]); //harf sayisi
	int sd;// = atoi(argv[3]); //random seed
	int threshold;//=atoi(argv[4]);//threshold to the state number. Above this, use preCostCompute otherwise use phiCost.
	int coefficient;//=atoi(argv[5]);
	int memoryusage;//=atoi(argv[6]); //maximum desired memory usage for preCostLevel in MBs

	int* automata; //= new int[P * N];

	string argm = argv[3];
	if (argm.find(".txt") != std::string::npos) {
		P = atoi(argv[2]);
		threshold = atoi(argv[4]);
		coefficient = atoi(argv[5]);
		memoryusage = atoi(argv[6]);
		automata =  new int[P * N];
		const char *directory = "../../automata_txt";
		char *mainDir = new char[512];
		char *dummy = getcwd(mainDir, 512);

		int ret = chdir(directory);
		if (ret == -1) {
			cout << "error when changing directory to read automaton from txt" << endl;
			return 1;
		}

		ifstream inputFile(argm);
		string temp;
		for (short i = 0; i < P; ++i) {
			for (short j = 0; j < N; ++j) {
				inputFile >> temp;
				automata[i + P * j] = atoi(temp.c_str());
			}
		}
		ret = chdir(mainDir);
		if (ret == -1) {
			cout << "error when changing directory after reading the automaton" << endl;
			delete[] mainDir;
			return 1;
		}
		delete[] mainDir;
	}
	else if((strcmp(argv[2], "cerny") == 0)) {
		P = 2;
		sd = 0;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];
		#pragma omp parallel
		{
			#pragma omp for schedule(static)
			for (int i = 0; i < P * N; i+=2) {
				automata[i] = i / 2;
				automata[i+1] = (i / 2 + 1) % N;
			} 
			automata[0] = 1;
		}
	}
	else if((strcmp(argv[2], "bactrian") == 0)) { // need odd N
		P = 2;
		sd = 0;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];
		automata[0] = N-2;
		automata[1] = N-1;
		automata[2] = N-1;
		automata[3] = 0;
		for (int i = 4; i < P * N; i+=P) {
			automata[i] = i / P;
			automata[i+1] = (i / P - 1) % N;
		}

	}
	else if((strcmp(argv[2], "dromedary") == 0)) { // need N>4
		P = 3;
		sd = 0;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];

		automata[0] = 0;
		automata[1] = 0;
		automata[2] = 3;

		automata[3] = 0;
		automata[4] = 0;
		automata[5] = 0;

		automata[6] = 0;
		automata[7] = 1;
		automata[8] = 3;

		for (int i = 9; i < P * N; i+=P) {
			automata[i] = i / P;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
		}
		automata[P*N-1] = 2;
	}
	else if((strcmp(argv[2], "fix5") == 0)) {
		P = 5;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];
		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;
		automata[3] = 1;
		automata[4] = 2;

		automata[5] = 2;
		automata[6] = 2;
		automata[7] = 2;
		automata[8] = 3;
		automata[9] = 3;

		automata[10] = 3;
		automata[11] = 2;
		automata[12] = 3;
		automata[13] = 3;
		automata[14] = 3;

		for (int i = 15; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
			automata[i+3] = (i / P + 1) % N;
			automata[i+4] = (i / P + 1) % N;

		}	
	}
	else if((strcmp(argv[2], "fix4") == 0)) {
		P = 4;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];
		
		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;
		automata[3] = 2;

		automata[4] = 2;
		automata[5] = 2;
		automata[6] = 2;
		automata[7] = 3;

		automata[8] = 3;
		automata[9] = 2;
		automata[10] = 3;
		automata[11] = 3;

		for (int i = 12; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
			automata[i+3] = (i / P + 1) % N;

		}	
	}
	else if((strcmp(argv[2], "fix3") == 0)) {
		P = 3;
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];

		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;

		automata[3] = 2;
		automata[4] = 2;
		automata[5] = 3;

		automata[6] = 3;
		automata[7] = 2;
		automata[8] = 3;

		for (int i = 9; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
		}	
	}
	else if((strcmp(argv[2], "doubleCerny") == 0)) {
		P = 3;
		int M = N; 
		N = 2 * N; // first N come from original automaton, the rest are primed states
		threshold = atoi(argv[3]);
		coefficient=atoi(argv[4]);
		memoryusage=atoi(argv[5]);
		automata =  new int[P * N];

		for (int i = 0; i < P * M; i+=P) {
			automata[i] = i / P;
			automata[i+1] = i / P;
			automata[i+2] = i / P + M;
		}

		for (int i = P * M; i < P * N; i+=P) {
			automata[i] = (i / P) % M;
			automata[i+1] = (i / P + 1) % M;
			automata[i+2] = i / P;
		}
		automata[P*M] = 1;
	}
	else {
		P = atoi(argv[2]);
	 	sd = atoi(argv[3]);
		threshold = atoi(argv[4]);
		coefficient = atoi(argv[5]);
		memoryusage = atoi(argv[6]);
		automata =  new int[P * N];
		std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()
		gen.seed(sd);
		std::uniform_int_distribution<> dis(0, N-1);
		for (int i = 0; i < P * N; ++i) {
			automata[i] = dis(gen);
		}
	}

#ifdef LOG_LEVEL
	char* filename = new char[256];
	struct stat st = { 0 };
	if (argm.find(".txt") != std::string::npos) {
		sprintf(filename, "results_synchroP/%s", argv[3]);
	}
	else {
		sprintf(filename, "results_synchroP/%s_%s", argv[1], argv[2]);
	}
	if (stat("results_synchroP", &st) == -1) {
		mkdir("results_synchroP", 0700);
	}
#if LOG_LEVEL & TIME_ANALYSIS
	sprintf(filename, "%s_time_and_length", filename);
#endif
#if LOG_LEVEL & DATA_ANALYSIS
	sprintf(filename, "%s_data", filename);
#endif
#if LOG_LEVEL & LEVEL_ANALYSIS
	sprintf(filename, "%s_level", filename);
#endif
	sprintf(filename, "%s.csv", filename);
	out.open(filename, ios::app);
	free(filename);
#endif

	out << to_string(threshold) << " " << to_string(coefficient) << " " << to_string(memoryusage) << " ";
	/*char* filenameAutomata = new char[256];
	if((strcmp(argv[2], "cerny") == 0)) {
		sprintf(filenameAutomata, "automata/%s_%s_automata.txt", argv[1], argv[2]);
	} else {
		sprintf(filenameAutomata, "automata/%s_%s_%s_automata.txt", argv[1], argv[2], argv[3]);
	}

	ifstream inputFile(filenameAutomata);
	string temp;
	for (short i = 0; i < P; ++i) {
		for (short j = 0; j < N; ++j) {
			inputFile >> temp;
			automata[i + P * j] = atoi(temp.c_str());
		}
	}
	free(filenameAutomata);*/

#ifdef DEBUG
	printAutomata(automata, N, P);
#endif

	/*cout << P << " "<< N << endl;
	for(int i = 0; i < N; ++i) {
	for(int j = 0; j < P; ++j) {
	cout << automata[j][i] << " ";
	}
	cout << endl;
	}*/

	int* inv_automata_ptrs = new int[P * (N + 1)];
	int* inv_automata = new int[P * N];

	for (int i = 0; i < P; ++i) {
		int *a = &(automata[i]);
		int *ia = &(inv_automata[i * N]);
		int *iap = &(inv_automata_ptrs[i * (N + 1)]);

		memset(iap, 0, sizeof(int) * (N + 1));
		for (int j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
		for (int j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
		for (int j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
		for (int j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
	}

	checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);
 
#ifdef DEBUG
	printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif

	PNode *path;
	//sequential version
	if (true) {
	  cout << "sequential:" << endl;
	  path = NULL;
	  greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, threshold, coefficient, memoryusage);
	  
	  pathPrinter(automata, path, N, P);
	}

	return 0;
}
