#ifndef _NAIVE_H_
#define _NAIVE_H_

#define THREAD_PER_BLOCK 256
#define WARP_SIZE 32
unsigned int BLOCK_PER_GRID, THREAD_TOTAL, WARP_TOTAL;

#include <iostream>
#include <fstream>
#include <iomanip>
#include "global.h"
#include "limits.h"
#include <cfloat>
#include <cmath>

using namespace std;

enum AlgorithmType { topDown, bottomUp, hybrid };
enum CudaOpt { naive, memory, warp };

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

int intlog(double base, double x) {
	return (int)(log(x) / log(base));
}


template<bool memoryOpt>
void synchronizing_check(typeAutomata *a, typeAutomata N, short P, typeDistance *distance, unsigned long long int lvltotal) {
	int* levels = new int[lvltotal];
	memset(levels, 0, lvltotal * sizeof(int));

	for (typeAutomata i = 0; i < N; ++i) {
		for (typeAutomata j = 0; j <= i; ++j) {
			int id;
			if (memoryOpt)
				id = Id(i, j);
			else
				id = IdNaive(i, j, N);

			if (distance[id] == -1) {
				for (short p = 0; p < P; p++) {
					typeAutomata ts1 = a[p + i * P];
					typeAutomata ts2 = a[p + j * P];
					int tid;
					if (memoryOpt)
						tid = Id(ts1, ts2);
					else
						tid = IdNaive(ts1, ts2, N);
					cout << "tid " << tid << ": distance is " << distance[tid] << endl;
				}

				cout << "automata is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
				out << endl;
				exit(0);
				return;
			}
			else {
				levels[distance[id]]++;
			}
		}
	}

#ifdef DEBUG
	int lvl = 0;
	while (levels[lvl] > 0) {
		cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
	}
	cout << endl;
#endif

#if LOG_LEVEL & DATA_ANALYSIS
	out << levels[1];
	lvl = 2;
	while (levels[lvl] > 0) {
		out << "-" << levels[lvl++];
	}
#endif
	free(levels);
}

template<bool memoryOpt>
__global__ void calculatePairCost(const typeAutomata* __restrict__ d_a, const typeDistance* __restrict__ d_distance, const typeLetter* __restrict__ d_letter, typeAutomata* __restrict__ d_cp_actives, typeAutomata* __restrict__  d_cp_actives2, int* __restrict__  d_cp_active_marker, unsigned long long* __restrict__ d_minOfWarp, typePair* __restrict__ d_minIdOfWarp, unsigned long long* __restrict__ d_finalDistWarp, typeAutomata no_actives, typeAutomata N, short P, int* d_WARP_TOTAL)
{
	volatile __shared__ short active_counts[THREAD_PER_BLOCK / WARP_SIZE];

	int threadId = blockDim.x * blockIdx.x + threadIdx.x;
	int warpId = threadId / WARP_SIZE;
	int warpThreadId = threadId % WARP_SIZE;
	int position = warpId;
	int noOfPair = (no_actives * (no_actives + 1)) / 2;
	int blockWarpId = threadIdx.x / WARP_SIZE;

	int *my_d_cp_active_marker = d_cp_active_marker + warpId * N;
	typeAutomata *my_d_cp_actives2 = d_cp_actives2 + warpId * N;

	//reset the active marker belonging to each warp by all threads in parallel..
	if (warpId < (*d_WARP_TOTAL)) {
		for (typeAutomata i = warpThreadId; i < N; i += WARP_SIZE) {
			my_d_cp_active_marker[i] = -1;
		}
	}

	typePair minId = 0;

	unsigned long long int minCost = ULLONG_MAX;

	while (position < noOfPair && warpId < (*d_WARP_TOTAL)) {
		typeAutomata ii = ((int)(sqrt((2.0 * position) + 1.0) - 0.5));
		typeAutomata jj = (position - ((ii * (ii + 1)) / 2));
		if (ii != jj) {
			typePair id = Id(d_cp_actives[ii], d_cp_actives[jj]);

			typeAutomata cp_no_actives = no_actives;

			//copying initial actives to warp specific storage for active states
			for (typeAutomata k = warpThreadId; k < N; k += WARP_SIZE) {
				my_d_cp_actives2[k] = d_cp_actives[k];
			}

			if (true) {
				//applying the path to the active states
				typePair cp_id = id;
				while (d_distance[cp_id] > 0) {
					typeLetter let = d_letter[cp_id];
					const typeAutomata* let_a = d_a + N * let;

					// applying letter to corresponding active states for each thread in the warp
					for (typeAutomata k = warpThreadId; k < N; k += WARP_SIZE) {
						my_d_cp_actives2[k] = let_a[my_d_cp_actives2[k]];
					}

					typeAutomata s1 = s1fromId(cp_id);
					typeAutomata s2 = s2fromId(cp_id, s1);
					cp_id = Id(let_a[s1], let_a[s2]);
				}

				if(warpThreadId == 0) {
				  typeAutomata active_count = 0;
				  for (typeAutomata i = 0; i < cp_no_actives; i++) {
				    typeAutomata curr = my_d_cp_actives2[i];
				    if (my_d_cp_active_marker[curr] != position) {
				      my_d_cp_actives2[active_count++] = curr;
				      my_d_cp_active_marker[curr] = position;
			  	  }
			  	}
				  active_counts[blockWarpId] = active_count;
				}
				cp_no_actives = active_counts[blockWarpId];
				

				/*//mark in the sparse array
				for (typeAutomata i = warpThreadId; i < cp_no_actives; i += WARP_SIZE) {
					my_d_cp_active_marker[my_d_cp_actives2[i]] = position;
				}

				//reduce the number of active states                                                                                                                                                             
				if (warpThreadId == 0) {
					typeAutomata active_count = 0;
					for (int i = 0; i < N; i++) {
						if (my_d_cp_active_marker[i] == position) {
							my_d_cp_actives2[active_count++] = i;
						}
					}
					active_counts[blockWarpId] = active_count;
				}
				cp_no_actives = active_counts[blockWarpId];*/

#if ALGORITHM == SP || ALGORITHM == PL || ALGORITHM == FR
				// all threads calculate the total distance of final pairs cumulatively
				d_finalDistWarp[warpId] = 0;
				unsigned long long int my_cost = 0;
				for (typeAutomata i = warpThreadId; i < cp_no_actives; i += WARP_SIZE) {
					typeAutomata s1 = my_d_cp_actives2[i];
					for (int j = cp_no_actives - 1; j > i; j--) {
						typeAutomata s2 = my_d_cp_actives2[j];
						if(s1 < s2) {
						  my_cost += d_distance[((s2 * (s2 + 1)) / 2) + s1];
						} else {
					  	my_cost += d_distance[((s1 * (s1 + 1))/2) + s2];
						}
					}
				}
				atomicAdd(&d_finalDistWarp[warpId], my_cost);

				if (warpThreadId == 0) {
					unsigned long long totals = d_finalDistWarp[warpId];

#if ALGORITHM == SP
					if (minCost > totals) {
						minCost = totals;
						minId = id;
					}
#elif ALGORITHM == PL
					if (minCost > totals + d_distance[id]) {
						minCost = totals + d_distance[id];
						minId = id;
					}
#else
					if (minCost > totals * d_distance[id]) {
						minCost = totals * d_distance[id];
						minId = id;
					}
#endif
				}
#elif ALGORITHM == CR
				if (warpThreadId == 0) {
					if (minCost > cp_no_actives) {
						minCost = cp_no_actives;
						minId = id;
					}
				}
#endif
			}
		}
		position += (*d_WARP_TOTAL);
	}

	if (warpThreadId == 0 && warpId < noOfPair && warpId < (*d_WARP_TOTAL)) {
		d_minOfWarp[warpId] = minCost;
		d_minIdOfWarp[warpId] = minId;
	}
}

template<bool memoryOpt>
void greedyHeuristic_finding(typeAutomata *a, typeDistance *distance, typeLetter *letter, typeAutomata *actives, int * active_marker, typeAutomata N, short P, PNode* &path) {

#if ALGORITHM == SP
	cout << "The algorithm is SynchroP" << endl;
#elif ALGORITHM == PL
	cout << "The algorithm is SynchroPL" << endl;
#elif ALGORITHM == CR
	cout << "The algorithm is Cardinality" << endl;
#elif ALGORITHM == FR
	cout << "The algorithm is SynchroP-Fractional" << endl;
#endif

	PNode* last = NULL;
	memset(active_marker, 0, sizeof(int) * N);
	typePair noOfPair = (N * (N + 1)) / 2;

	bool* havePair = new bool[noOfPair];
	memset(havePair, false, sizeof(bool) * noOfPair);

	typeAutomata no_actives = N;
	for (typeAutomata i = 0; i < N; ++i) {
		actives[i] = i;
	}

	typeAutomata* cp_actives = new typeAutomata[N];
	int* cp_active_marker = new int[N];

	unsigned long long* minOfWarp = new unsigned long long[WARP_TOTAL];
	unsigned long long* d_minOfWarp;
	cudaMalloc((void**)&d_minOfWarp, WARP_TOTAL * sizeof(unsigned long long));
	gpuErrchk(cudaDeviceSynchronize());

	typePair* minIdOfWarp = new typePair[WARP_TOTAL];
	typeAutomata *d_a, *d_cp_actives, *d_cp_actives2;
	typeLetter *d_letter;
	int *d_cp_active_marker;
	typePair *d_minIdOfWarp;
	unsigned long long *d_finalDistWarp;
	typeDistance *d_distance;
	int* d_WARP_TOTAL;

	typeAutomata *a_transposed = new typeAutomata[N*P];
	for (typeAutomata i = 0; i < N; i++) {
		for (short j = 0; j < P; j++) {
			a_transposed[i + j*N] = a[j + i*P];
		}
	}

	int maxLevel = 0;
	for (typePair idd = 0; idd < noOfPair; idd++)
	{
		maxLevel = max(maxLevel, distance[idd]);
	}

	cudaMalloc((void**)&d_a, N * P * sizeof(typeAutomata));
	cudaMalloc((void**)&d_cp_actives, N * sizeof(typeAutomata));
	cudaMalloc((void**)&d_cp_actives2, N * WARP_TOTAL * sizeof(typeAutomata));
	cudaMalloc((void**)&d_cp_active_marker, N * WARP_TOTAL * sizeof(int));
	cudaMalloc((void**)&d_distance, noOfPair * sizeof(typeDistance));
	cudaMalloc((void**)&d_letter, noOfPair * sizeof(typeLetter));
	cudaMalloc((void**)&d_minIdOfWarp, WARP_TOTAL * sizeof(typePair));
	cudaMalloc((void**)&d_finalDistWarp, WARP_TOTAL * sizeof(unsigned long long));
	cudaMalloc((void**)&d_WARP_TOTAL, sizeof(int));

	gpuErrchk(cudaDeviceSynchronize());

	cudaMemcpy(d_a, a_transposed, N * P * sizeof(typeAutomata), cudaMemcpyHostToDevice);
	cudaMemcpy(d_distance, distance, noOfPair * sizeof(typeDistance), cudaMemcpyHostToDevice);
	cudaMemcpy(d_letter, letter, noOfPair * sizeof(typeLetter), cudaMemcpyHostToDevice);
	cudaMemcpy(d_WARP_TOTAL, &WARP_TOTAL, sizeof(int), cudaMemcpyHostToDevice);
	gpuErrchk(cudaDeviceSynchronize());

	int step = 1;
	while (no_actives > 1) {
		//double eet1 = omp_get_wtime();
		// 
		//cudaDeviceSynchronize();
		//cout << omp_get_wtime()-eet1 << " -- " << endl;
		//cout << "no active states is " << no_actives << endl;
		//find the pair id with minimum phi-cost value 
		unsigned long long int min_cost = ULLONG_MAX;

		typePair min_id;
		cudaMemcpy(d_cp_actives, actives, N* sizeof(typeAutomata), cudaMemcpyHostToDevice);

		double it1 = omp_get_wtime();
		//cout << "kernel starts" << endl;
		calculatePairCost<true> << <BLOCK_PER_GRID, THREAD_PER_BLOCK >> >(d_a, d_distance, d_letter, d_cp_actives, d_cp_actives2, d_cp_active_marker, d_minOfWarp, d_minIdOfWarp, d_finalDistWarp, no_actives, N, P, d_WARP_TOTAL);
		gpuErrchk(cudaDeviceSynchronize());
		double it2 = omp_get_wtime();
		cout << "kernel is done in " << it2 - it1 << " seconds - no actives " << no_actives << endl;
		gpuErrchk(cudaPeekAtLastError());

    cudaMemcpy(minOfWarp, d_minOfWarp, WARP_TOTAL * sizeof(unsigned long long), cudaMemcpyDeviceToHost);
		cudaMemcpy(minIdOfWarp, d_minIdOfWarp, WARP_TOTAL * sizeof(typePair), cudaMemcpyDeviceToHost);

		// if the number of warps is greater than the number of active pairs, some warps stood idle during kernel execution
		// so we should ignore their uninitialized values, countFullComputed gives the number of warps that worked
		int countFullComputed = (WARP_TOTAL < (no_actives * (no_actives + 1)) / 2) ? (WARP_TOTAL) : ((no_actives * (no_actives + 1)) / 2);
		for (int i = 0; i < countFullComputed; i++) // traverse all the minimum costs from threads and find the minimum of minimums
		{
			if (min_cost > minOfWarp[i])
			{
				min_cost = minOfWarp[i];
				min_id = minIdOfWarp[i];
			}
		}
		cout << min_id << " " << min_cost << " ";

#if LOG_LEVEL & DATA_ANALYSIS
		if (max_of_min_distances < distance[min_id])
			max_of_min_distances = distance[min_id];
		min_distance_counter++;
		min_distance_sum += distance[min_id];
#endif
		// cout << "merging pair from level " << min_distance << endl;

		//apply the path and store it
		typePair pid = min_id;
		int added = 0;
		while (distance[pid] > 0) {
			typeLetter let = letter[pid];
			insertToPath(let, path, last);
			added++;

			for (typeAutomata i = 0; i < no_actives; i++) {
				actives[i] = a[let + actives[i] * P];
			}

			typeAutomata s1, s2;

			if (memoryOpt) {
				s1 = s1fromId(pid);
				s2 = s2fromId(pid, s1);
				pid = Id(a[let + s1 * P], a[let + s2 * P]);
			}
			else {
				s1 = pid / N;
				s2 = pid % N;
				pid = IdNaive(a[let + s1 * P], a[let + s2 * P], N);
			}
		}

		cout << added << endl;

		//reduce the number of active states
		typeAutomata active_count = 0;
		for (typeAutomata i = 0; i < no_actives; i++) {
			typeAutomata act = actives[i];
			if (active_marker[act] != step) {
				actives[active_count++] = act;
				active_marker[act] = step;
			}
		}
		no_actives = active_count;
		step++;
	}


	free(cp_actives);
	free(cp_active_marker);
	free(minOfWarp);
	free(minIdOfWarp);
	cudaFree(d_a);
	cudaFree(d_cp_actives);
	cudaFree(d_cp_actives2);
	cudaFree(d_cp_active_marker);
	cudaFree(d_distance);
	cudaFree(d_letter);
	cudaFree(d_minOfWarp);
	cudaFree(d_minIdOfWarp);
	cudaFree(d_finalDistWarp);
}

//a is automata a[i][j] -> state j goes to a[i][j] with letter j
//iap is inverse automata pointers -> ia[i][iap[i][j] ... ia[i][j+1]] keeps the state ids which go to state j with letter i
//there are N states and p letters in the automata
void greedyHeuristic_naive(typeAutomata* a, typeAutomata* iap, typeAutomata* ia, typeAutomata N, short P, PNode* &path, unsigned long long int lvltotal) {
	typePair noOfPair = (N * (N + 1)) / 2;
	
	size_t l_free = 0;
    size_t l_Total = 0;
    cudaError_t error_id = cudaMemGetInfo(&l_free, &l_Total);

	unsigned long long memory = l_free;
	int maxThreads = (memory - 2*N*P - 2*N - 6*noOfPair - 4) / (12 + 6*N);

	THREAD_TOTAL = min(maxThreads, 32 * N * N);
	BLOCK_PER_GRID = (THREAD_TOTAL + THREAD_PER_BLOCK - 1) / THREAD_PER_BLOCK;
	WARP_TOTAL = (THREAD_TOTAL + WARP_SIZE - 1) / WARP_SIZE;

	/*cout << memory << " " << maxThreads << endl;
	cout << THREAD_PER_BLOCK << " " << BLOCK_PER_GRID << " " << THREAD_TOTAL << endl;*/
	
	typeAutomata* actives = new typeAutomata[N];
	typeDistance* distance = new typeDistance[noOfPair];
	typeLetter* letter = new typeLetter[noOfPair];
	int* que = new int[noOfPair];

	int* active_marker = new int[noOfPair];


#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif

#if LOG_LEVEL & DATA_ANALYSIS
	int max_of_min_distances = 0;
	int min_distance_counter = 0;
	int min_distance_sum = 0;
#endif


	for (typePair i = 0; i < noOfPair; i++) {
		distance[i] = -1;
	}

	//BFS queue for the pairs
	int qs = 0;
	int qe = 0;

	for (int i = 0; i < N; ++i) {
		typePair id = Id(i, i);
		distance[id] = 0;
		que[qe++] = id;
	}

	//there are more nodes in the queue
	while (qs < qe) {
		int q_id = que[qs++];
		int q_dist = distance[q_id];

		//will process the pair with id q_id now
		typeAutomata q_s1 = s1fromId(q_id); //the first state in the pair
		typeAutomata q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif

		typeAutomata* p_ia = ia; //this is the inverse automata for letter p
		typeAutomata* p_iap = iap; //and its state pointers

		for (short p = 0; p < P; p++) {

			for (typeAutomata iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
				typeAutomata ia_s1 = p_ia[iap_s1_ptr];
				for (typeAutomata iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
					typeAutomata ia_s2 = p_ia[iap_s2_ptr];
					typePair ia_id = Id(ia_s1, ia_s2);
					if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
						distance[ia_id] = q_dist + 1;
						letter[ia_id] = p;
						que[qe++] = ia_id;
					}
				}
			}
			p_ia += N; //this is the inverse automata for letter p
			p_iap += (N + 1); //and its state pointers
		}
	}
#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << time << " ";
#endif
#endif

	synchronizing_check<true>(a, N, P, distance, lvltotal);

#ifdef TIMER
	t1 = omp_get_wtime();
#endif

	greedyHeuristic_finding<true>(a, distance, letter, actives, active_marker, N, P, path);

#ifdef TIMER
	time = omp_get_wtime() - t1;
	//cout << "Path generation takes " << time << " seconds\n";
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << total << " ";
#endif
#endif
#if LOG_LEVEL & DATA_ANALYSIS
	out << " " << (N * (N - 1)) / 2 << " " << lvl - 1 << " "
		<< max_of_min_distances << " " << float(min_distance_sum) / min_distance_counter;
#endif
	free(distance);
	free(letter);
	free(que);
	free(actives);
	free(active_marker);
}

#endif //_NAIVE_H_
