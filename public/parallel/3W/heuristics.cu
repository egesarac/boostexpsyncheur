#define SP 1
#define PL 2
#define CR 3
#define FR 4

#ifndef ALGORITHM
#define ALGORITHM SP
#warning Algorithm not given explicitly, SynchroP "SP" assumed
#endif

#include <iostream>
#include <fstream>
#include <cmath>
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "omp.h"
#include "global.h"
#include "naive.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>

using namespace std;

bool is_empty(std::ifstream& pFile) {
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int checkInverse(typeAutomata *a, typeAutomata* iap, typeAutomata* ia, typeAutomata N, short P) {
	for (short p = 0; p < P; p++) {
		for (typeAutomata i = 0; i < N; i++) {
			typeAutomata target = a[p + i * P];

			short found = 0;
			for (typeAutomata iaptr = iap[p * (N + 1) + target]; iaptr < iap[p * (N + 1) + target + 1]; ++iaptr) {
				typeAutomata incoming = ia[p * N + iaptr];
				if (i == incoming) {
					found = 1;
					break;
				}
			}

			if (!found) {
				cout << "something is wrong " << i << " goes to " << target << " with " << p << " but it is not in the inverse automata\n";
				exit(1);
			}
		}
	}

	for (short p = 0; p < P; p++) {
		for (typeAutomata i = 0; i < N; i++) {
			for (typeAutomata iaptr = iap[p * (N + 1) + i]; iaptr < iap[p * (N + 1) + i + 1]; ++iaptr) {
				int source = ia[p * N + iaptr];
				if (a[p + source * P] != i) {
					cout << "something is wrong " << i << " has " << source << " in inverse automata but it " << source << " goes to " << a[p + source * P] << " with " << p << "\n";
					exit(1);
				}
			}
		}
	}

	return 0;
}

int main(int argc, char** argv) {
	if ((!(argc == 4 || (argc == 3 && (strcmp(argv[2], "cerny") == 0 || strcmp(argv[2], "bactrian") == 0 || strcmp(argv[2], "dromedary") == 0 || strcmp(argv[2], "fix5") == 0 || strcmp(argv[2], "fix4") == 0 || strcmp(argv[2], "fix3") == 0 || strcmp(argv[2], "doubleCerny") == 0))))) {
		cout << "Usage: " << argv[0] << " no_states alphabet_size rand_seed\n" << endl;
		cout << "Usage: " << argv[0] << " no_states alphabet_size file_name\n" << endl;
		cout << "Usage: " << argv[0] << " no_states slowly_sync_automata_type\n" << endl;
		cout << "slowly_sync_automata_type: cerny, bactrian, dromedary, fix5, fix4, fix3, doubleCerny\n" << endl;
		return 1;
	  }
	short N = atoi(argv[1]);
	short P;
	uint seed;
	string argm;
	if (argc == 4) {
		argm = argv[3];
	}

 	cudaSetDevice(0);
 	cudaDeviceSynchronize();
	short* pdummy = NULL;
	cudaMalloc((void**)&pdummy, sizeof(short));
	cudaDeviceSynchronize();

#ifdef LOG_LEVEL
	char* filename = new char[256];

	struct stat st = { 0 };

#if ALGORITHM == SP
	if (argm.find(".txt") != std::string::npos) {
		sprintf(filename, "results_synchroP/%s", argv[3]);
	}
	else {
		sprintf(filename, "results_synchroP/%s_%s", argv[1], argv[2]);
	}
	if (stat("results_synchroP", &st) == -1) {
		mkdir("results_synchroP", 0700);
	}
#elif ALGORITHM == PL
	if (argm.find(".txt") != std::string::npos) {
		sprintf(filename, "results_synchroPL/%s", argv[3]);
	}
	else {
		sprintf(filename, "results_synchroPL/%s_%s", argv[1], argv[2]);
	}
	if (stat("results_synchroPL", &st) == -1) {
		mkdir("results_synchroPL", 0700);
	}
#elif ALGORITHM == CR
	if (argm.find(".txt") != std::string::npos) {
		sprintf(filename, "results_cardinality/%s", argv[3]);
	}
	else {
		sprintf(filename, "results_cardinality/%s_%s", argv[1], argv[2]);
	}
	if (stat("results_cardinality", &st) == -1) {
		mkdir("results_cardinality", 0700);
	}
#elif ALGORITHM == FR
	if (argm.find(".txt") != std::string::npos) {
		sprintf(filename, "results_fractional/%s", argv[3]);
	}
	else {
		sprintf(filename, "results_fractional/%s_%s", argv[1], argv[2]);
	}
	if (stat("results_fractional", &st) == -1) {
		mkdir("results_fractional", 0700);
	}
#else
#error Algorithm is not properly defined to be one of these: "SP", "PL", "CR", "FR"
#endif


#if LOG_LEVEL & TIME_ANALYSIS
	sprintf(filename, "%s_time_and_length", filename);
#endif
#if LOG_LEVEL & DATA_ANALYSIS
	sprintf(filename, "%s_data", filename);
#endif
#if LOG_LEVEL & LEVEL_ANALYSIS
	sprintf(filename, "%s_level", filename);
#endif
	sprintf(filename, "%s.csv", filename);
	out.open(filename, ios::app);
	free(filename);
#endif

	unsigned long long int lvltotal = N * (N-1) / 2;
	typeAutomata* automata;
	
	if (argm.find(".txt") != std::string::npos) {
		P = atoi(argv[2]);
		automata =  new typeAutomata[P * N];
		const char *directory = "../../automata_txt";
		char *mainDir = new char[512];
		char *dummy = getcwd(mainDir, 512);

		int ret = chdir(directory);
		if (ret == -1) {
			cout << "error when changing directory to read automaton from txt" << endl;
			return 1;
		}

		ifstream inputFile(argv[3]);
		string temp;
		for (short i = 0; i < P; ++i) {
			for (short j = 0; j < N; ++j) {
				inputFile >> temp;
				automata[i + P * j] = atoi(temp.c_str());
			}
		}
		ret = chdir(mainDir);
		if (ret == -1) {
			cout << "error when changing directory after reading the automaton" << endl;
			delete[] mainDir;
			return 1;
		}
		delete[] mainDir;
	}
	else if((strcmp(argv[2], "cerny") == 0)) {
		P = 2;
		seed = 0;
		automata =  new typeAutomata[P * N];
		#pragma omp parallel
		{
			#pragma omp for schedule(static)
			for (int i = 0; i < P * N; i+=2) {
				automata[i] = i / 2;
				automata[i+1] = (i / 2 + 1) % N;
			} 
			automata[0] = 1;
		}
	}
	else if((strcmp(argv[2], "bactrian") == 0)) { // need odd N
		P = 2;
		lvltotal = (N+1) * N / 2;
		automata =  new typeAutomata[P * N];
		automata[0] = N-2;
		automata[1] = N-1;
		automata[2] = N-1;
		automata[3] = 0;
		for (int i = 4; i < P * N; i+=P) {
			automata[i] = i / P;
			automata[i+1] = (i / P - 1) % N;
		}

	}
	else if((strcmp(argv[2], "dromedary") == 0)) { // need N>4
		P = 3;
		automata =  new typeAutomata[P * N];

		automata[0] = 0;
		automata[1] = 0;
		automata[2] = 3;

		automata[3] = 0;
		automata[4] = 0;
		automata[5] = 0;

		automata[6] = 0;
		automata[7] = 1;
		automata[8] = 3;

		for (int i = 9; i < P * N; i+=P) {
			automata[i] = i / P;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
		}
		automata[P*N-1] = 2;
	}
	else if((strcmp(argv[2], "fix5") == 0)) {
		P = 5;
		automata =  new typeAutomata[P * N];
		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;
		automata[3] = 1;
		automata[4] = 2;

		automata[5] = 2;
		automata[6] = 2;
		automata[7] = 2;
		automata[8] = 3;
		automata[9] = 3;

		automata[10] = 3;
		automata[11] = 2;
		automata[12] = 3;
		automata[13] = 3;
		automata[14] = 3;

		for (int i = 15; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
			automata[i+3] = (i / P + 1) % N;
			automata[i+4] = (i / P + 1) % N;

		}	
	}
	else if((strcmp(argv[2], "fix4") == 0)) {
		P = 4;
		automata =  new typeAutomata[P * N];
		
		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;
		automata[3] = 2;

		automata[4] = 2;
		automata[5] = 2;
		automata[6] = 2;
		automata[7] = 3;

		automata[8] = 3;
		automata[9] = 2;
		automata[10] = 3;
		automata[11] = 3;

		for (int i = 12; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
			automata[i+3] = (i / P + 1) % N;

		}	
	}
	else if((strcmp(argv[2], "fix3") == 0)) {
		P = 3;
		automata =  new typeAutomata[P * N];

		automata[0] = 1;
		automata[1] = 0;
		automata[2] = 2;

		automata[3] = 2;
		automata[4] = 2;
		automata[5] = 3;

		automata[6] = 3;
		automata[7] = 2;
		automata[8] = 3;

		for (int i = 9; i < P * N; i+=P) {
			automata[i] = (i / P + 1) % N;;
			automata[i+1] = i / P;
			automata[i+2] = (i / P + 1) % N;
		}	
	}
	else if((strcmp(argv[2], "doubleCerny") == 0)) {
		P = 3;
		int M = N; 
		N = 2 * N; // first N come from original automaton, the rest are primed states
		lvltotal = N * (N-1) / 2;
		automata =  new typeAutomata[P * N];

		for (int i = 0; i < P * M; i+=P) {
			automata[i] = i / P;
			automata[i+1] = i / P;
			automata[i+2] = i / P + M;
		}

		for (int i = P * M; i < P * N; i+=P) {
			automata[i] = (i / P) % M;
			automata[i+1] = (i / P + 1) % M;
			automata[i+2] = i / P;
		}
		automata[P*M] = 1;
	}
	else {
		P = atoi(argv[2]);
		seed = atoi(argv[3]);
		lvltotal = 200;
		automata =  new typeAutomata[P * N];
		#pragma omp parallel
		{
			uint tseed = (omp_get_thread_num() + 1) * (seed + 1912812);
			#pragma omp for schedule(static)
			for (int i = 0; i < P * N; ++i) {
				automata[i] = ((int)rand_r(&tseed)) % N;
			}
		}
		#if LOG_LEVEL & AUTOMATA_ANALYSIS
			if (stat("automata", &st) == -1) {
				mkdir("automata", 0700);
			}
			char* filenameAutomata = new char[256];
			sprintf(filenameAutomata, "automata/%s_%s_%s_automata.txt", argv[1], argv[2], argv[3]);			
			aout.open(filenameAutomata, ios::app);
		#endif
	}

#ifdef AUTOMATA_ANALYSIS
	ifstream inputFile(filenameAutomata);
	if (is_empty(inputFile)) {
		for (short i = 0; i < P; ++i) {
			for (short j = 0; j < N; ++j) {
				aout << automata[i + P * j] << " ";
			}
			aout << endl;
		}
		aout.close();
	}
	else {
		string temp;
		for (short i = 0; i < P; ++i) {
			for (short j = 0; j < N; ++j) {
				inputFile >> temp;
				automata[i + P * j] = atoi(temp.c_str());
			}
		}
	}
	free(filenameAutomata);
#endif

#ifdef DEBUG
	printAutomata(automata, N, P);
#endif

	typeAutomata* inv_automata_ptrs = new typeAutomata[P * (N + 1)];
	typeAutomata* inv_automata = new typeAutomata[P * N];

#pragma omp parallel for schedule(static)
	for (typeAutomata i = 0; i < P; ++i) {
		typeAutomata *a = &(automata[i]);
		typeAutomata *ia = &(inv_automata[i * N]);
		typeAutomata *iap = &(inv_automata_ptrs[i * (N + 1)]);

		memset(iap, 0, sizeof(typeAutomata) * (N + 1));
		for (typeAutomata j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
		for (typeAutomata j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
		for (typeAutomata j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
		for (typeAutomata j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
	}

	checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);

#ifdef DEBUG
	printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif

	PNode *path;
	if (true) {
		cout << "sequential:" << endl;
		path = NULL;
		greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path, lvltotal);
		pathPrinter(automata, path, N, P);
	}

#ifdef LOG_LEVEL
	out << endl;
	out.close();
#endif

	return 0;
}
