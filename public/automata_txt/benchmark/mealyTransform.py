#!curl https://automata.cs.ru.nl/automata_pmwiki/uploads/Main/models.Mealy.zip --output mealy.zip
#!unzip mealy.zip
#!mkdir Mealy_Converted

# With this code, we check whether we have any non-unique file names.
# If we see 'NOT UNIQUE' output, it is not.
# Rest of the code depends on the uniqueness of the file names, because we put the resulting files in a single folder.
# Also, we create a map of files to file paths
EXTENSION = '.dot'

import os
files_to_paths_map = {}
for root, dirs, files in os.walk('Mealy'):
    for file in files:
        if file.endswith(EXTENSION) and 'BenchmarkCircuits' not in root:
            if file in files_to_paths_map:
                print('NOT UNIQUE')
                exit(1)
            else:
                files_to_paths_map[file] = os.path.join(root, file)

incomplete_automatons = []
nondeterministic_automatons = []
invalid_formats = []

UNDETERMINED_TRANSITION = -1

for filename, path in files_to_paths_map.items():
    infile = open(path, 'r')
    is_incomplete = False
    is_nondeterministic = False
    transitions = []
    states = []
    letters = []
    states_inverse_map = {}
    letters_inverse_map = {}
    matrix = []
    for line in infile:
        if '->' in line and 'label="' in line:
            line = line.strip()
            state_part = line.split('[')[0].split('->')
            if len(state_part) != 2:
                print(line)
                raise ValueError('There are not two states around the arrow. File path: ' + path)
            
            s1 = state_part[0].strip()
            s2 = state_part[1].strip()

            label_part = line.split('label="')[1]
            if '"' not in label_part:
                raise ValueError('There is no closing quotation mark for label. File path: ' + path)
            
            label_part = label_part.split('"')[0]

            letter = label_part.split('/')[0].strip() # we may or may not have a slash, but this work regardless.

            transitions.append( (s1, s2, letter) )

    infile.close()

    for transition in transitions:
        s1, _, letter = transition

        if s1 not in states_inverse_map:
            states_inverse_map[s1] = len(states)
            states.append(s1)
        if letter not in letters_inverse_map:
            letters_inverse_map[letter] = len(letters)
            letters.append(letter)

    for transition in transitions:
        _, s2, _ = transition
        if s2 not in states_inverse_map:
            is_incomplete = True
            break

    if is_incomplete == True:
        incomplete_automatons.append( (filename, path) )
        continue

    for i in range(len(letters)):
        matrix.append([-1] * len(states))

    for transition in transitions:
        s1, s2, letter = transition
        s1_index = states_inverse_map[s1]
        s2_index = states_inverse_map[s2]
        letter_index = letters_inverse_map[letter]

        if matrix[letter_index][s1_index] != UNDETERMINED_TRANSITION:
            is_nondeterministic = True
            break

        matrix[letter_index][s1_index] = s2_index

    if is_nondeterministic == True:
        nondeterministic_automatons.append( (filename, path) )
        continue

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == UNDETERMINED_TRANSITION:
                is_incomplete = True

    if is_incomplete == True:
        incomplete_automatons.append( (filename, path) )
        continue

    if len(states) == 0 or len(letters) == 0:
        invalid_formats.append( (filename, path) )
        continue

    outputfilename = str(len(states)) + '_' + str(len(letters)) + '_' + filename[:len(filename) - len(EXTENSION)] + '.txt'

    outfile = open(os.path.join('Mealy_Converted', outputfilename), 'w')

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            outfile.write( str(matrix[i][j]) + " " )
        outfile.write(os.linesep)

    outfile.close()
        
    print('Converted:', path)


f = open('nondeterministic_automatons.txt', 'w')
for item in nondeterministic_automatons:
    f.write(item[1])
    f.write(os.linesep)
f.close()

f = open('incomplete_automatons.txt', 'w')
for item in incomplete_automatons:
    f.write(item[1])
    f.write(os.linesep)
f.close()

f = open('invalid_formats.txt', 'w')
for item in invalid_formats:
    f.write(item[1])
    f.write(os.linesep)
f.close()

#!zip -r Mealy_Converted.zip Mealy_Converted/.
#!zip -ur Mealy_Converted.zip nondeterministic_automatons.txt
#!zip -ur Mealy_Converted.zip incomplete_automatons.txt
#!zip -ur Mealy_Converted.zip invalid_formats.txt