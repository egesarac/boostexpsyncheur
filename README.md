### Boosting Expensive Synchronizing Heuristics ###

This repository contains the implementations and the test data used in [1].

* You can compile each implementation by using the included Makefile.
* For CPU implementations, there are several parameters used for fine-tuning the algorithmic improvements described in Section 4 of [1]. We set *threshold = 100*, *levelCoefficient = 1*, and *memoryUsage = 8000 (i.e., 8 GBs)* in our experiments.
* To run CPU implementations on random automata, use `./heuristics numberOfStates alphabetSize randomSeed threshold levelCoefficient memoryUsage`.
* To run CPU implementations on an automaton given as a text file, use `./heuristics numberOfStates alphabetSize fileName.txt threshold levelCoefficient memoryUsage`. You need to move the corresponding text file to the automata_txt folder (from the subfolders or anywhere else).
* To run CPU implementations on slowly synchronizing automata, use `./heuristics numberOfStates automataType threshold levelCoefficient memoryUsage`.
* GPU implementations do not require the additional parameters above. However, we have various heuristics implemented as discussed in Section 5 of [1]. For each heuristic *x* in {*sp*, *pl*, *fr*, *cr*}, you can run these implementations as follows.
* To run GPU implementations on random automata, use `./heuristics_x numberOfStates alphabetSize randomSeed`.
* To run GPU implementations an automaton given as a text file, , use `./heuristics_x numberOfStates alphabetSize fileName.txt`. You need to move the corresponding text file to the automata_txt folder (from the subfolders or anywhere else).
* To run GPU implementations on slowly synchronizing automata, use `./heuristics_x numberOfStates automataType`.
* For slowly-synchronizing automata, we have 7 options: *cerny* [2]; *bactrian*, *dromedary* [3]; *fix5*, *fix4*, *fix3* [4]; *doubleCerny* [5]. Note that, if *doubleCerny* is run with *numberOfStates = n*, the implementation performs the transformation described in [5] to obtain a slowly-synchronizing automaton with *2n* states from the Černý automaton with *n* states.
* The text files need to contain the transition table of the automaton where the first row lists the transitions with letter *a*, and the first column lists the transitions from state 0, and so on. See the example file *4_cerny_automata.txt*. 

## References
[1]
N. E. Saraç, Ö. F. Altun, K. T. Atam, S. Karahoda, K. Kaya, and H. Yenigün (2020).
Boosting Expensive Synchronizing Heuristics.
Expert Systems with Applications.

[2]
M. V. Volkov (2008).
Synchronizing Automata and the Černý Conjecture.
Language and Automata Theory and Applications.

[3]
D. S. Ananichev, M. V. Volkov, Y. I. Zaks (2006).
Synchronizing Automata with a Letter of Deficiency 2.
Developments in Language Theory.

[4]
H. Don, H. Zantema, M. de Bondt (2020).
Slowly Synchronizing Automata with Fixed Alphabet Size.
Computing Research Repository.

[5]
M. V. Volkov (2018).
Slowly Synchronizing Automata with Idempotent Letters of Low Rank.
Computing Research Repository